﻿using UnityEngine;
using System.Collections;

public class ObstacleBehaviour : MonoBehaviour {
	
	private float range;
	private float timeStart;
	private float rangeY;
	
	public GameObject obstacle;

	private float waitTime;
		
	void Start () {
		timeStart = 0;
		range = 1;
	}
	
	
	void Update () {

		if (GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().slowed == true)
			waitTime = 1.2f;
		else
			waitTime = 0;

		if (Time.time > (timeStart + range + waitTime)) {
			timeStart = Time.time + range;

			if(GameObject.Find("Player").GetComponent<PlayerBehaviour>().dead == false)
				Generate ();

			range /= GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().Speed / 125;
		}
	}

	void Generate()
	{
		rangeY = Random.Range (0.5f, 1.45f);
		obstacle.transform.localScale = new Vector3 (obstacle.transform.localScale.x,rangeY,obstacle.transform.localScale.z);
		
		Instantiate (obstacle, new Vector3 (0, -7, 0), Quaternion.identity);
		range = Random.Range (0.76f,1.0f);
	}
}