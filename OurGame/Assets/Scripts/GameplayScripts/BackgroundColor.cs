﻿using UnityEngine;
using System.Collections;

public class BackgroundColor : MonoBehaviour {

	SpriteRenderer spriteRenderer;
	private Color colorGreen; private bool changeToGreen;
	private Color colorBlue; private bool changeToBlue;
	private Color colorPurple; private bool changeToPurple;
	private Color colorRed; private bool changeToRed;
	private Color colorYellow; private bool changeToYellow;

	private float duration = 5;
	private float t = 0;


	void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
		colorGreen = new Color32(51, 216, 125, 255);
		colorBlue = new Color32(98, 218, 224, 255);
		colorPurple = new Color32(189, 161, 216, 255);
		colorRed = new Color32 (234, 97, 116, 255);
		colorYellow = new Color32 (145, 216, 51, 255);


		changeToBlue = true;
	}

	void FixedUpdate()
	{

		//CHANGING TO BLUE
		if (GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().changeBgColor == true && changeToBlue == true) {

			spriteRenderer.color = Color.Lerp (spriteRenderer.color, colorBlue , t);

			if(t < 1)
				t+= Time.deltaTime / duration;

			if(spriteRenderer.color == colorBlue){
				GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().changeBgColor = false;
				t = 0;
				changeToBlue = false;
				changeToPurple = true;
			}
				
		}
			
		//CHANGING TO PURPLE
		if (GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().changeBgColor == true && changeToPurple == true) {
			spriteRenderer.color = Color.Lerp (spriteRenderer.color, colorPurple , t);

			if(t < 1)
				t += Time.deltaTime/ duration;

			if(spriteRenderer.color == colorPurple)
			{
				GameObject.Find ("Player").GetComponent<PlayerBehaviour>().changeBgColor = false;
				t = 0;
				changeToPurple = false;
				changeToRed = true;
			}
		}

		//CHANGING TO RED

		if (GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().changeBgColor == true && changeToRed == true) {
			spriteRenderer.color = Color.Lerp (spriteRenderer.color, colorRed , t);

			if(t < 1)
				t += Time.deltaTime/duration;

			if(spriteRenderer.color == colorRed)
			{
				GameObject.Find ("Player").GetComponent<PlayerBehaviour>().changeBgColor = false;
				t = 0;
				changeToRed = false;
				changeToYellow = true;
			}
		}

		//CHANGINH TO YELLOW

		if (GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().changeBgColor == true && changeToYellow == true) {
			spriteRenderer.color = Color.Lerp (spriteRenderer.color, colorYellow , t);
			
			if(t < 1)
				t += Time.deltaTime/duration;
			
			if(spriteRenderer.color == colorYellow)
			{
				GameObject.Find ("Player").GetComponent<PlayerBehaviour>().changeBgColor = false;
				t = 0;
				changeToYellow = false;
				changeToGreen = true;
			}
		}

		//CHANGING TO GREEN

		if (GameObject.Find ("Player").GetComponent<PlayerBehaviour> ().changeBgColor == true && changeToGreen == true) {
			spriteRenderer.color = Color.Lerp (spriteRenderer.color, colorGreen , t);
			
			if(t < 1)
				t += Time.deltaTime/duration;
			
			if(spriteRenderer.color == colorGreen)
			{
				GameObject.Find ("Player").GetComponent<PlayerBehaviour>().changeBgColor = false;
				t = 0;
				changeToGreen = false;
				changeToBlue = true;
			}
		}
	}
}
