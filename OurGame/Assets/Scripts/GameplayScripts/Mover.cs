﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {


	void Update () 
	{
		Rigidbody2D rigidbody = GetComponent<Rigidbody2D> ();
		
		rigidbody.velocity = Vector3.up * GameObject.Find ("Player").GetComponent<PlayerBehaviour>().Speed * Time.deltaTime;
	}
}
