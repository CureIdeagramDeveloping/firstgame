﻿using UnityEngine;
using System.Collections;

public class newHighscoreFlash : MonoBehaviour {

	private Color startColor;
	// Use this for initialization
	void Start () {
		startColor = this.GetComponent<TextMesh> ().color;
		InvokeRepeating ("flashToWhite", 0, 1);
		InvokeRepeating ("flashFromWhite", 0.5f, 1);
	}
	
	// Update is called once per frame
	void Update () {

	}

	void flashToWhite()
	{
		this.GetComponent<TextMesh> ().color = Color.Lerp (this.GetComponent<TextMesh> ().color, new Color(255,255,255,255), Time.deltaTime * 10);
	}

	void flashFromWhite()
	{
		this.GetComponent<TextMesh> ().color = Color.Lerp (this.GetComponent<TextMesh> ().color, startColor, Time.deltaTime * 10);
	}
}
