﻿using UnityEngine;
using System.Collections;

public class FasterTextMover : MonoBehaviour {

	private float Speed;

	void Update()
	{
		Speed = 300.0f;

		Rigidbody2D rigidbody = GetComponent<Rigidbody2D> ();

		if(GameObject.Find ("Player").GetComponent<PlayerBehaviour>().dead == false)
			rigidbody.velocity = Vector3.left * Speed * Time.deltaTime;
	}
}
