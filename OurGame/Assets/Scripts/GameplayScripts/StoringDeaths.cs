﻿using UnityEngine;
using System.Collections;

public class StoringDeaths : MonoBehaviour {

	public int deathCounter;

	void Awake()
	{
		DontDestroyOnLoad (this);

		if (FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (gameObject);
		}
	}
}
