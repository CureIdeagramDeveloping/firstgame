﻿using UnityEngine;
using System.Collections;

public class ShrinkOnExit : MonoBehaviour {

	public GameObject player;
	public bool canShrink;
	public GameObject score;
	public int ourScore = 0;

	void Start()
	{
		canShrink = true;
	}

	void OnTriggerExit2D(Collider2D other)
	{
		ourScore++;
		canShrink = false;
	}

	void Update()
	{
		if (canShrink == false && player.transform.localScale.x < 0.9f) {
			player.transform.localScale += new Vector3 (0.1f, 0.1f, 0.1f);
			player.transform.rotation = Quaternion.identity;
			
			score.GetComponent<TextMesh>().text = ourScore.ToString();

		}
	}


}
