﻿using UnityEngine;
using System.Collections;

public class AddForce : MonoBehaviour {

	public GameObject fallingPiece;

	void FixedUpdate()
	{
		fallingPiece.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-5, 5), Random.Range (-5,5)), ForceMode2D.Impulse);
		fallingPiece.transform.Rotate (new Vector3(0,0,3) * 500 * Time.deltaTime);

		fallingPiece.GetComponent<SpriteRenderer> ().color = Color.Lerp (fallingPiece.GetComponent<SpriteRenderer> ().color, new Color(255,255,255,0), Time.deltaTime * 2);
	}
}
