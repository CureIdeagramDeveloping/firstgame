﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class PlayerBehaviour : MonoBehaviour 
{
	public GameObject player;
	public float Speed;
	private float checking;
	public GameObject gameOverText;
	public GameObject fasterText;
	public GameObject restartText;
	public GameObject highscoreText;
	public GameObject newHighText;
	public GameObject yourHighscore;
	public GameObject slowerText;

	public GameObject fallingPiece1;
	public GameObject fallingPiece2;
	public GameObject fallingPiece3;
	public GameObject fallingPiece4;
	public GameObject fallingPiece5;
	public GameObject fallingPiece6;
	public GameObject fallingPiece7;


	private bool newHigh = false;
	public bool dead = false;

	private float restartTimer;
	
	private int randomChance;

	public bool slowed = false;
	public int fasterCounter = 0;

	private AudioSource source;
	public AudioClip hit;
	public AudioClip swoosh;

	private bool startedShrink = false;

	public bool changeBgColor = false;

	void Awake()
	{
		if (Advertisement.isSupported) {
			Advertisement.allowPrecache = true;
			Advertisement.Initialize("32858", false);
		}

	}
	
	void Start()
	{
		source = GetComponent<AudioSource>();
		checking = Time.time + 10;
	}
	
	void Update()
	{
		if (GameObject.Find ("StoringDeaths").GetComponent<StoringDeaths>().deathCounter % 7 == 0) {
			Advertisement.Show(null, new ShowOptions {
				pause = true,
				resultCallback = result => {
					Debug.Log(result.ToString());
					GameObject.Find ("StoringDeaths").GetComponent<StoringDeaths>().deathCounter = 1;
				}
			});
		}

		if (Time.time >= checking && dead != true) {

			randomChance = Random.Range(0,2);

			if((randomChance == 0 && slowed == false && Speed != 160) || fasterCounter == 3)
			{
				Instantiate(slowerText, new Vector3(6, 4.1f , -1),Quaternion.identity);
				Speed = 80.0f;
				slowed = true;
				checking += 2;
				fasterCounter = 0;
			}
			else
			{
				if(slowed == true)
				{
					Instantiate(fasterText, new Vector3(6, 4.1f , -1),Quaternion.identity);
					Speed = 250.0f;
					slowed = false;
					checking += 10;
					fasterCounter++;
					changeBgColor = true;
				}
				else
				{
					if(fasterCounter == 2){
						changeBgColor = true;
						Instantiate(fasterText, new Vector3(6, 4.1f , -1),Quaternion.identity);
						Speed *= 1.23f;
						slowed = false;
						checking += 10;
						fasterCounter++;
					}
					else{
						changeBgColor = true;
						Instantiate(fasterText, new Vector3(6, 4.1f , -1),Quaternion.identity);
						Speed *= 1.33f;
						slowed = false;
						checking += 10;
						fasterCounter++;
					}

				}
			}
		}

		if (dead) 
		{
			if (Input.GetMouseButton (0) && player.transform.localScale.x >= 0) 
			{
				player.transform.localScale = new Vector3 (0.9f, 0.9f, 0.9f);
			} 
			else if (!Input.GetMouseButton (0) && player.transform.localScale.x < 0.9f) 
			{
				player.transform.localScale = new Vector3(0.9f,0.9f, 0.9f);
			}
		} 
		else 
		{
			if(Input.GetMouseButtonDown(0))
				AudioSource.PlayClipAtPoint(swoosh, player.transform.position, 0.12f);

			if (Input.GetMouseButton (0) && player.transform.localScale.x >= 0) 
			{
				if(GameObject.Find ("ColliderOfPlayer").GetComponent<ShrinkOnExit>().canShrink == true)
				{
					player.transform.localScale -= new Vector3 (0.1f, 0.1f, 0.1f);
					player.transform.Rotate((Vector3.forward * 600) * Time.deltaTime);
				}
				
				if(Input.GetMouseButtonDown(0))
					GameObject.Find ("ColliderOfPlayer").GetComponent<ShrinkOnExit>().canShrink = true;
			} 
			else if (!Input.GetMouseButton (0) && player.transform.localScale.x < 0.9f) 
			{
				player.transform.localScale += new Vector3(0.1f,0.1f, 0.1f);
				player.transform.rotation = Quaternion.identity;
			}
		}

		if (dead) {

			if(Input.GetMouseButtonDown(0) && Time.time > restartTimer)
			{
				Application.LoadLevel (Application.loadedLevel);
			}
		}
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Prepreka") {

			GameObject.Find ("StoringDeaths").GetComponent<StoringDeaths>().deathCounter++;

			Speed = 0.0f;
			dead = true;

			source.PlayOneShot(hit);

			StoreHighscore(GameObject.Find ("ColliderOfPlayer").GetComponent<ShrinkOnExit>().ourScore);

			player.transform.position = new Vector3(0,-10,0);

			Instantiate(fallingPiece1, new Vector3(0, 3, 0), Quaternion.identity);
			Instantiate(fallingPiece2, new Vector3(0, 3, 0), Quaternion.identity);
			Instantiate(fallingPiece3, new Vector3(0, 3, 0), Quaternion.identity);
			Instantiate(fallingPiece4, new Vector3(0, 3, 0), Quaternion.identity);
			Instantiate(fallingPiece5, new Vector3(0, 3, 0), Quaternion.identity);
			Instantiate(fallingPiece6, new Vector3(0, 3, 0), Quaternion.identity);
			Instantiate(fallingPiece7, new Vector3(0, 3, 0), Quaternion.identity);

			Instantiate(gameOverText, new Vector3(6,0,-1), Quaternion.identity);
			Instantiate(restartText, new Vector3(6, -0.5f, -1), Quaternion.identity);
			Instantiate (highscoreText, new Vector3(6,-1.8f,-1), Quaternion.identity);

			if(newHigh)
				Instantiate (newHighText, new Vector3(6,-3,-1), Quaternion.identity);
			else
				Instantiate (yourHighscore, new Vector3(6,-2.48f,-1), Quaternion.identity);

			restartTimer = Time.time + 0.6f;
		}
	}

	void StoreHighscore(int newHighscore)
	{
		int oldHighscore = PlayerPrefs.GetInt("highscore", 0);    
		if (newHighscore > oldHighscore) {
			newHigh = true;
			PlayerPrefs.SetInt ("highscore", newHighscore);
		}

	}
}
