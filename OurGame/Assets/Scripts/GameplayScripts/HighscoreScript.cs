﻿using UnityEngine;
using System.Collections;

public class HighscoreScript : MonoBehaviour {

	public GameObject target;
	private int highScore;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		highScore = PlayerPrefs.GetInt ("highscore");
		target.GetComponent<TextMesh> ().text = highScore.ToString ();
	}
}
