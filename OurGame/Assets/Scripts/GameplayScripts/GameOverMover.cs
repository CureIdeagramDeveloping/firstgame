﻿using UnityEngine;
using System.Collections;

public class GameOverMover : MonoBehaviour {

	public GameObject gameOverText;
	private float Speed;

	void Start()
	{
		Speed = 1000.0f;
	}

	void Update()
	{
		Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();

		if (rigidbody.position.x > 0.2 && rigidbody.position.x < 0.8) {
			Speed = 0;
		}
		else 
		{
			rigidbody.velocity = Vector3.left * Speed * Time.deltaTime;

			if(gameOverText.transform.localScale.y <= 1)
			{
				gameOverText.transform.localScale += new Vector3(0, 0.03f, 0);
			}
		}
			
	}


}
