﻿using UnityEngine;
using System.Collections;

public class RotatingPlayer : MonoBehaviour {

	public GameObject rectText;
	public GameObject tapText;
	private Quaternion newRotationRect;
	private bool rotateRectAnimationFirst = false;
	private bool rotateRectAnimationSecond = false;
	
	private bool pressed;

	void Start()
	{
		InvokeRepeating ("RotateRectFirst", 0, 1);
		InvokeRepeating ("RotateRectSecond", 0.5f, 1);
		newRotationRect = rectText.transform.rotation;
	}
	
	void Update()
	{
		if (rotateRectAnimationFirst) {
			rectText.transform.rotation = Quaternion.Slerp (rectText.transform.rotation, newRotationRect, Time.deltaTime * 3);
		}

		if (rotateRectAnimationSecond) {
			rectText.transform.rotation = Quaternion.Lerp (rectText.transform.rotation, newRotationRect, Time.deltaTime * 3);
		}

		if (!pressed) {
			transform.Rotate ((Vector3.forward * 60) * Time.deltaTime);

		}

		if (Input.GetMouseButtonDown (0)) {

			pressed = true;				
			InvokeRepeating("LoadLevel", 0.6f, 0.0f);
		}

		if (pressed) {
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime * 10);
			rectText.GetComponent<MeshRenderer> ().material.color = Color.Lerp (rectText.GetComponent<MeshRenderer> ().material.color, new Color(0,0,0,0), Time.deltaTime * 10);
			tapText.GetComponent<MeshRenderer> ().material.color = Color.Lerp (tapText.GetComponent<MeshRenderer> ().material.color, new Color(0,0,0,0), Time.deltaTime * 10);
		}
			
	}

	void LoadLevel()
	{
		Application.LoadLevel (1);
	}

	void RotateRectFirst()
	{
		newRotationRect.z *= -1;
		rotateRectAnimationSecond = false;
		rotateRectAnimationFirst = true;
	}

	void RotateRectSecond()
	{
		newRotationRect.z *= -1;
		rotateRectAnimationSecond = true;
		rotateRectAnimationFirst = false;
	}
}
